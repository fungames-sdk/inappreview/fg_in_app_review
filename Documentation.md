Introduction
------------------------------------------------------------------------------
The FG InAppReview plugin handles the in app review process within a Unity application. It will help you gather feedback from users that are actively engaged with your game. It will provide the following capabilities:

- Request In App Reviews
- Track different stages of the review process through callbacks
    - Review Triggered
    - Review Request Sent
    - Review Request Failure
    - Review Request Completion
    - Review Launch Sent
    - Review Launch Failure
    - Review Launch Completion

Scripting API
------------------------------------------------------------------------------
Once an InAppReview sub module is added to your project, you will be able to handle in app reviews using the FGInAppReviewManager static methods and callbacks:

RequestInAppReview()

This is an example on how to use it:

FGInAppReview.RequestInAppReview();

You can also subscribe to these callbacks if you want to attach custom actions at initialization or when an event is sent:

FGInAppReview.Callbacks.Initialization;
FGInAppReview.Callbacks.OnInitialized;
FGInAppReview.Callbacks.TriggerReviewEvent;
FGInAppReview.Callbacks.InAppReviewRequestEvent;
FGInAppReview.Callbacks.InAppReviewRequestFailedEvent;
FGInAppReview.Callbacks.InAppReviewRequestCompletedEvent;
FGInAppReview.Callbacks.InAppReviewLaunchEvent;
FGInAppReview.Callbacks.InAppReviewLaunchFailedEvent;
FGInAppReview.Callbacks.InAppReviewLaunchCompletedEvent;

Here is an example of how to subscribe to one of these callbacks:

FGInAppReview.Callbacks.InAppReviewRequestEvent += OnReviewRequest;
